const fs = require('fs');


function callback1(board, ids = "mcu453ed", callback) {
    setTimeout(() => {
        fs.readFile(board, 'utf-8', (err, data) => {
            if (err) {
                callback(err);
            } else {
                let answer = JSON.parse(data).filter((values) => {
                    return values.id == ids;
                });
                callback(null, answer);
            }
        })
    }, 2 * 1000);
}

module.exports = callback1;