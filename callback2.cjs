const fs = require('fs');
//const path = require('path');
//const problem1 = require('../callback1.cjs');



function callback2(list, ids, callback) {
    setTimeout(() => {
        fs.readFile(list, 'utf-8', (err, data) => {
            if (err) {
                callback(err);
            } else {
                let value = JSON.parse(data)[ids];
                callback(null, value);

            }
        })
    }, 2 * 1000);
}

module.exports = callback2;