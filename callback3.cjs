const fs = require('fs');
//const path = require('path');


function callback3(card, id, callback) {
    setTimeout(() => {
        fs.readFile(card, 'utf-8', (err, data) => {
            if (err) {
                callback3(err);
            }
            else {
                let answer = JSON.parse(data)[id];
                callback(null, answer);
            }
        })
    }, 2 * 1000)
}

//callback3(card,"cffv432",callback)
module.exports = callback3;