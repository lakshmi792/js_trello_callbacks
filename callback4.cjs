const fs = require('fs');
const path = require('path');

const problem3 = require('./callback3.cjs');
const problem1 = require('./callback1.cjs');
const problem2 = require('./callback2.cjs');

function callback4(list, board, cards, thanos) {
    setTimeout(() => {
        problem1(board, thanos, (err, data) => {
            if (err) {
                console.error(err);
            } else {
                console.log(data);
                problem2(list, thanos, (err, data) => {
                    if (err) {
                        console.error(err);
                    } else {
                        console.log(data);
                        const mind = data.find((element) => {
                            return element.name == 'Mind';
                        });
                        problem3(cards, mind.id, (err, data) => {
                            if (err) {
                                console.error(err);
                            } else {
                                console.log(data);
                            }
                        })
                    }
                })
            }
        })

    }, 2 * 1000);
}


module.exports = callback4;