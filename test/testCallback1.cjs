const fs = require('fs');
const path = require('path');
const problem1 = require('../callback1.cjs');

const board = path.resolve('../boards.json');

function callback(err, value) {
    if (err) {
        console.error(err);
    }
    else {
        console.log("successfully read the file")
        console.log(value);
        
    }
}

problem1(board,"mcu453ed",callback);