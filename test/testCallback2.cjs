const fs = require('fs');
const path = require('path');
const problem2 = require('../callback2.cjs');

const list = path.resolve('../lists.json');

function callback(err, values) {
    if (err) {
        console.error(err);
    }
    else {
        console.log(values);
    }
}

problem2(list, "mcu453ed", callback)