const fs = require('fs');
const path = require('path');
const problem3 = require('../callback3.cjs');

const card = path.resolve('../cards.json');

function callback(err,value){
    if(err){
        console.error(err);
    }
    else{
        console.log(value);
    }
}

problem3(card,"qwsa221",callback);