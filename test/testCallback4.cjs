const path = require('path');
const problem4 = require('../callback4.cjs');

const board = path.resolve(__dirname,'../boards.json');
const list = path.resolve(__dirname,'../lists.json');
const cards = path.resolve(__dirname,'../cards.json');

problem4(list,board,cards,'mcu453ed');