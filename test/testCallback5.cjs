const path = require('path');
const problem5 = require('../callback5.cjs');

const board = path.resolve(__dirname,'../boards.json');
const list = path.resolve(__dirname,'../lists.json');
const cards = path.resolve(__dirname,'../cards.json');

problem5(list,board,cards,'mcu453ed');

