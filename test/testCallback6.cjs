const path = require('path');
const problem6 = require('../callback6.cjs');

const board = path.resolve(__dirname,'../boards.json');
const list = path.resolve(__dirname,'../lists.json');
const cards = path.resolve(__dirname,'../cards.json');

problem6(list,board,cards,'mcu453ed');
